package jimbonet.uk.wordle;

import java.util.Arrays;
import java.util.Locale;
import java.util.Random;
import java.util.ResourceBundle;

public class Wordbank {
    private String[] wordList;
    private String[] allowedWords;
    public Wordbank() {
        Init();
    }

    private void Init() {
        ResourceBundle bundle = ResourceBundle.getBundle("wordle");
        this.wordList = bundle.getString("wordlist").split(",");
        this.allowedWords = bundle.getString("allowedwords").split(",");
    }

    public String Random() {
        int rnd = new Random().nextInt(wordList.length);
        return wordList[rnd];
    }

    public Boolean Validate(String word) {
        word = word.toLowerCase();
        Boolean result = Arrays.asList(wordList).contains(word) | Arrays.asList(allowedWords).contains(word);
        return result;
    }
}
