package jimbonet.uk.wordle;

public class Word {
    public String Word;
    public WordMap wordMap;

    public Word(String word) {
        this.Word = word;
        this.wordMap = new WordMap(Word);
    }

    public String Guess(String guessWord) {
        wordMap.Reset();
        WordMap guessMap = new WordMap(guessWord);
        String greenResult = "";
        String yellowResult = "";
        String result = "";
        for(Letter guessLetter:guessMap.map) {
            greenResult += GuessLetterGreen(guessLetter);
        }
        for(Letter guessLetter:guessMap.map) {
            yellowResult += GuessLetterYellow(guessLetter);
        }
        for(int i = 0, n = greenResult.length() ; i < n ; i++) {
            if (greenResult.charAt(i) == '-' & yellowResult.charAt(i) == 'Y') {
                result += yellowResult.charAt(i);
            }
            else {
                result += greenResult.charAt(i);
            }
        }
        return result;
    }

    private String GuessLetterGreen(Letter guessLetter) {
        String result = "-";
        for (Letter wordLetter:wordMap.map) {
            result = "-";
            if (!wordLetter.Processed) {
                if (wordLetter.Letter == guessLetter.Letter) {
                    if (wordLetter.Position == guessLetter.Position) {
                        result = "G";
                        wordLetter.Processed = true;
                    }
                    break;
                }
            }
        }
        return result;
    }

    private String GuessLetterYellow(Letter guessLetter) {
        String result = "-";
        for (Letter wordLetter:wordMap.map) {
            result = "-";
            if (!wordLetter.Processed) {
                if (wordLetter.Letter == guessLetter.Letter) {
                    if (wordLetter.Position != guessLetter.Position) {
                        result = "Y";
                        wordLetter.Processed = true;
                    }
                    break;
                }
            }
        }
        return result;
    }
}
