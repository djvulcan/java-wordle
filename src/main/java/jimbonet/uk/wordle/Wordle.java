package jimbonet.uk.wordle;

import java.util.ArrayList;
import java.util.List;

public class Wordle {
    public static void main(String[] args) {
        System.out.println("J-WORDLE\n========\n\nBy James O'Hara, 2022\n");
        new Game();
    }
}

class WordMap {
    public List<Letter> map;
    public WordMap(String Word) {
        this.map = new ArrayList<Letter>();
        String word = Word.toUpperCase();
        for(int i = 0, n = word.length() ; i < n ; i++) {
            map.add(new Letter(word.charAt(i), false, i));
        }
    }

    public void Reset() {
        for(Letter letter:map) {
            letter.Processed = false;
        }
    }
}

class Letter {
    public char Letter;
    public boolean Processed;
    public int Position;
    public Letter(char letter, Boolean processed, int position) {
        this.Letter = letter;
        this.Processed = processed;
        this.Position = position;
    }

    public void Reset() {
        this.Processed = false;
    }
}
