package jimbonet.uk.wordle;

class OutputTools {
    OutputTools() {

    }
    public String Wordlify(String guessOutput) {
        String wordleOutput = "";
        for(int i = 0, n = guessOutput.length() ; i < n ; i++) {
            String character = "⬛";
            if (guessOutput.charAt(i) == 'Y') {
                character = "\uD83D\uDFE8";
            }
            if (guessOutput.charAt(i) == 'G') {
                character = "🟩";
            }
            wordleOutput += character;
        }
        return wordleOutput;
    }

    public String WordleGuess(String guessOutput, String guessWord) {
        String wordleOutput = "";
        for(int i = 0, n = guessWord.length() ; i < n ; i++) {
            String character = "\033[1;37;100m" + guessWord.charAt(i);
            if (guessOutput.charAt(i) == 'Y') {
                character = "\033[1;37;103m" + guessWord.charAt(i);
            }
            if (guessOutput.charAt(i) == 'G') {
                character = "\033[1;37;102m" + guessWord.charAt(i);
            }
            wordleOutput += character;
        }
        wordleOutput += "\033[0m";
        return wordleOutput;
    }
}
