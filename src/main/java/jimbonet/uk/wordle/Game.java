package jimbonet.uk.wordle;

import java.util.Scanner;

class Game {
    int turns;
    Word thisWord;
    Wordbank wordBank;
    String result;
    Game() {
        OutputTools outputTools = new OutputTools();
        result = "You ran out of turns.  Better luck next time.";
        wordBank = new Wordbank();
        thisWord = new Word(wordBank.Random());
        turns = 6;
        Scanner input = new Scanner(System.in);
        String transcript = "";
        for(int i = 1, n = turns ; i < n ; i++) {
            Boolean valid = false;
            String guess = "";
            while (!valid) {
                System.out.println("Guess a word  (" + i + "/6):\n");
                guess = input.next().toUpperCase();
                valid = wordBank.Validate(guess);
                if (!valid) {
                    System.out.println("Invalid word! Try again");
                }
            }
            valid = false;
            String guessResult = thisWord.Guess(guess);
            transcript += outputTools.Wordlify(guessResult) + "\n";
            System.out.println(outputTools.WordleGuess(guessResult, guess));
            if (guessResult.equals("GGGGG")) {
                result = "Congratulations! You got it in " + i + "/6";
                break;
            }
        }
        System.out.println(result);
        System.out.println(transcript);
    }
}
