package jimbonet.uk.wordle;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class TestWordle {
    @Test
    public void TestValidate() {
        Wordbank wordbank = new Wordbank();
        assertTrue(wordbank.Validate("james"));
        assertTrue(wordbank.Validate("JAMES"));
        assertFalse(wordbank.Validate("aaaaa"));
    }

    @Test
    public void TestCorrectGuess() {
        Word testWord = new Word("ALLOW");
        String testGuess = testWord.Guess("ALLOW");
        assertEquals("GGGGG", testGuess);
    }

    @Test
    public void TestInCorrectGuess() {
        Word testWord = new Word("ALLOW");
        String testGuess = testWord.Guess("QTERT");
        assertEquals("-----", testGuess);
    }

    @Test
    public void SomeCorrectLetters() {
        Word testWord = new Word("AMLOW");
        String testGuess = testWord.Guess("EFLOT");
        assertEquals("--GG-", testGuess);
    }

    @Test
    public void ImCorrectPlacesLetters() {
        Word testWord = new Word("ABCDE");
        String testGuess = testWord.Guess("BADEC");
        assertEquals("YYYYY", testGuess);
    }

    @Test
    public void DuplicateCorrectPlacesLetters() {
        Word testWord = new Word("FERET");
        String testGuess = testWord.Guess("FFPOT");
        assertEquals("G---G", testGuess);
    }

    @Test
    public void GreenBeforeYellowLetters() {
        Word testWord = new Word("ALLOW");
        String testGuess = testWord.Guess("ALOOF");
        assertEquals("GG-G-", testGuess);
    }

    @Test
    public void GreenBeforeYellowWithOtherYellowLetters() {
        Word testWord = new Word("ALLOW");
        String testGuess = testWord.Guess("AWOOF");
        assertEquals("GY-G-", testGuess);
    }
}
