# J-Wordle
by James O'Hara

## To install:

Note requires Java RE >= 17

1. Browser to https://gitlab.com/djvulcan/java-wordle/-/packages
2. Select the latest Version
3. Download the `wordle-<version>.jar` file from the **files** section


## To Run:
Type `java -jar wordle-<version>.jar` then enter

## To Unit Test:
1. Clone this repo and change to the cloned folder
2. `./gradlew test`
